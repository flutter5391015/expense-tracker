import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

String formatDate(DateTime date) {
  final formatter = DateFormat('dd/MMMM/yyyy');
  return formatter.format(date);
}

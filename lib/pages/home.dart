import 'package:expense_app/models/expense.dart';
import 'package:expense_app/widgets/expense_list.dart';
import 'package:flutter/material.dart';
import 'package:expense_app/data/dummy_expense.dart';
import 'package:expense_app/widgets/expense_add_modal.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  void openAddExpenseOverlay() {
    showModalBottomSheet(
        isDismissible: false,
        isScrollControlled: true,
        context: context,
        builder: (ctx) {
          return SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: ExpenseAddModal(
                onSaveExpense: addExpenseHandler,
              ),
            ),
          );
        });
  }

  final GlobalKey<AnimatedListState> _listKey = GlobalKey<AnimatedListState>();
  void addExpenseHandler(Expense expense) {
    setState(() {
      registeredExpense.add(expense);
      int newIndex = registeredExpense.length - 1;
      _listKey.currentState!.insertItem(newIndex);
    });
  }

  void removeExpenseHandler(Expense expense, int index) {
    setState(() {
      _listKey.currentState!.removeItem(
        index,
        (context, animation) => SizeTransition(
          sizeFactor: animation,
        ),
      );
      registeredExpense.remove(expense);
    });

    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        behavior: SnackBarBehavior.floating,
        backgroundColor: Color.fromARGB(255, 91, 255, 97),
        duration: Duration(seconds: 3),
        content: Text(
          'Data deleted',
          style: TextStyle(
            color: Color.fromARGB(210, 0, 0, 0),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget mainContent = const Center(
      child: Text('No Expense found. Start Add some !'),
    );

    if (registeredExpense.isNotEmpty) {
      mainContent = ExpenseList(
        keyList: _listKey,
        expense: registeredExpense,
        onRemoveHandler: removeExpenseHandler,
      );
    }

    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        centerTitle: false,
        title: const Align(
          alignment: Alignment.centerLeft,
          child: Text(
            'Flutter Expense App',
          ),
        ),
        actions: [
          IconButton(
            onPressed: openAddExpenseOverlay,
            icon: const Icon(Icons.add),
          ),
        ],
      ),
      body: Container(
        padding: const EdgeInsets.all(4),
        child: Column(
          children: [
            Expanded(
              child: mainContent,
            )
          ],
        ),
      ),
    );
  }
}

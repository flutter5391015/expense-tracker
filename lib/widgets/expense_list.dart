import 'package:expense_app/models/expense.dart';
import 'package:expense_app/widgets/expense_list_item.dart';
import 'package:flutter/material.dart';

class ExpenseList extends StatefulWidget {
  final List<Expense> expense;
  final void Function(Expense expense, int index) onRemoveHandler;
  final GlobalKey<AnimatedListState> keyList;
  const ExpenseList(
      {super.key,
      required this.expense,
      required this.onRemoveHandler,
      required this.keyList});

  @override
  State<ExpenseList> createState() => _ExpenseListState();
}

class _ExpenseListState extends State<ExpenseList> {
  @override
  Widget build(BuildContext context) {
    return AnimatedList(
      key: widget.keyList,
      initialItemCount: widget.expense.length,
      itemBuilder: (context, index, animation) {
        return SizeTransition(
          sizeFactor: animation,
          child: Dismissible(
            direction: DismissDirection.endToStart,
            onDismissed: (direction) {
              widget.onRemoveHandler(
                widget.expense[index],
                index,
              );
            },
            key: ValueKey(widget.expense[index]),
            child: ExpenseListItem(
              key: ValueKey(widget.expense[index]),
              expense: widget.expense[index],
            ),
          ),
        );
      },
    );
  }
}

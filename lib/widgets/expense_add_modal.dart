import 'package:expense_app/models/expense.dart';
import 'package:expense_app/utils/formatDate.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ExpenseAddModal extends StatefulWidget {
  const ExpenseAddModal({super.key, required this.onSaveExpense});

  final void Function(Expense expense) onSaveExpense;

  @override
  State<ExpenseAddModal> createState() => _ExpenseAddModalState();
}

class _ExpenseAddModalState extends State<ExpenseAddModal> {
  final titleController = TextEditingController();
  final amountController = TextEditingController();
  var categoryController = Category.leisure;

  DateTime? dateTime;

  @override
  void dispose() {
    // TODO: implement dispose
    titleController.dispose();
    super.dispose();
  }

  void saveDate() {
    if (titleController.text.trim().isEmpty) {
      if (Theme.of(context).platform == TargetPlatform.iOS) {
        showCupertinoDialog(
          context: context,
          builder: (ctx) => CupertinoAlertDialog(
            title: const Text('Error'),
            content: const Text('Please Input text first'),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.pop(ctx);
                },
                child: const Text('Close'),
              )
            ],
          ),
        );
      } else {
        showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: const Text('Error'),
            content: const Text('Please Input text first'),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.pop(ctx);
                },
                child: const Text('Close'),
              )
            ],
          ),
        );
      }
      return;
    } else if (amountController.text.trim().isEmpty ||
        double.tryParse(amountController.text)! <= 0) {
      if (Theme.of(context).platform == TargetPlatform.iOS) {
        showCupertinoDialog(
          context: context,
          builder: (ctx) => CupertinoAlertDialog(
            title: const Text('Error'),
            content: const Text('Please Input amount first'),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.pop(ctx);
                },
                child: const Text('Close'),
              ),
            ],
          ),
        );
      } else {
        showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: const Text('Error'),
            content: const Text('Please Input amount first'),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.pop(ctx);
                },
                child: const Text('Close'),
              )
            ],
          ),
        );
      }
      return;
    } else if (dateTime == null) {
      if (Theme.of(context).platform == TargetPlatform.iOS) {
        showCupertinoDialog(
          context: context,
          builder: (ctx) => CupertinoAlertDialog(
            title: const Text('Error'),
            content: const Text('Please select date first'),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.pop(ctx);
                },
                child: const Text('Close'),
              ),
            ],
          ),
        );
      } else {
        showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: const Text('Error'),
            content: const Text('Please select date first'),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.pop(ctx);
                },
                child: const Text('Close'),
              )
            ],
          ),
        );
      }
      return;
    }

    widget.onSaveExpense(
      Expense(
          title: titleController.text,
          amount: double.tryParse(amountController.text)!,
          date: dateTime!,
          category: categoryController),
    );

    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    //alternatif untuk cek tinggi dari keyboard;
    //final keyboardHeight = MediaQuery.of(context).viewInsets.bottom;

    return Container(
      padding: const EdgeInsets.fromLTRB(16, 48, 16, 16),
      child: Column(mainAxisSize: MainAxisSize.min, children: [
        TextField(
          controller: titleController,
          maxLength: 10,
          decoration: const InputDecoration(
            label: Text('Title'),
          ),
        ),
        Row(
          children: [
            Expanded(
              child: TextField(
                controller: amountController,
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly
                ],
                keyboardType: TextInputType.number,
                decoration: const InputDecoration(
                  prefixText: '\$',
                  label: Text('Amount'),
                ),
              ),
            ),
            const SizedBox(
              width: 16,
            ),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    dateTime == null
                        ? 'Selected Date'
                        : formatDate(
                            dateTime!,
                          ),
                  ),
                  IconButton(
                    onPressed: () {
                      final now = DateTime.now();
                      final firstDate =
                          DateTime(now.year - 1, now.month - 1, now.day);
                      showDatePicker(
                              context: context,
                              initialDate: now,
                              firstDate: firstDate,
                              lastDate: now)
                          .then(
                        (value) => setState(
                          () => dateTime = value,
                        ),
                      );
                    },
                    icon: const Icon(Icons.calendar_month),
                  )
                ],
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            DropdownButton(
                value: categoryController,
                items: Category.values
                    .map((category) => DropdownMenuItem(
                          value: category,
                          child: Text(
                            category.name.toString().toUpperCase(),
                          ),
                        ))
                    .toList(),
                onChanged: (value) {
                  setState(() {
                    if (value != null) categoryController = value;
                  });
                }),
            Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                ElevatedButton(
                  onPressed: saveDate,
                  child: const Text('Save'),
                ),
                const SizedBox(
                  width: 16,
                ),
                TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('Cancel'),
                )
              ],
            )
          ],
        )
      ]),
    );
  }
}

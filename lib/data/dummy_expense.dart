import 'package:expense_app/models/expense.dart';

final List<Expense> registeredExpense = [
  Expense(
      title: 'Flutter Course',
      amount: 19.9,
      date: DateTime.now(),
      category: Category.work),
  Expense(
      title: 'Cinema',
      amount: 15.4,
      date: DateTime.now(),
      category: Category.leisure),
];
